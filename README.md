# README #


## About ##

This CustomerService repository is part of an ongoing R&D/prototyping project: a microservices-based e-commerce application with each service implemented with a different stack (Spring Boot, Micronaut, Serverless, Node.js).  
When the customer-, inventory-, order-, notification- and payment-services are 'done' i�m planning to tie them all together in an async choreographed manner using RabbitMQ (or Kafka), Eureka, Spring Cloud Configuration, API Gateway, and possibly Keycloak for authentication.


## Tech ##

- Back-end: Java 17, Spring Boot 2.5.4, Spring Data, Hibernate Validator, REST, MongoDB, Lombok, Flogger, JUnit 5, Mockito, Hamcrest, Docker, Docker-compose, Spring Boot Layertools (Docker image optimization), Maven, NGINX, Tomcat, Postman, Git, Bitbucket, IntelliJ.
- Front-end: Vue.js 3, Vue-router 4, Axios 0.21, Vuelidate 2, Oruga 0.5.2 (Toasts), Vue-fontawesome, Javascript ES6/ES2015, NPM, Bootstrap 4.6, HTML, CSS, Docker, Visual Studio Code/VSCode


## Status ##

- Wrapping up the client errorhandling code
- CRUD REST API and admin UI functionality done
- Now waiting for the other services to complete before adding the messaging connector  
  

## Future development ##

- Add robust message sending/listening capabities to the services
- Orchestrate/choreaograph a simple order flow
- Add authentication
- Add a service mesh to manage inter-service communication, for example Istio.
- Expose the data via a GraphQL API

- Side project: migrate from Docker-compose to Kubernetes


## Build & Run ##

**Front-end and back-end combined: production mode using Docker-compose**

1. In a console/terminal tab, cd to the project root (/microshopcustomerservice)

2. Build the Docker images  
$ docker-compose build

3. Run the Docker-compose cluster:  
$ docker-compose up  
*Note: if the API URL differs from http://localhost:8081/api, change the environment variable named VUE_APP_API_BASE_URL in docker-compose.yml accordingly.*

4. Access the following URL in your browser. Note the different port, since we're going to look at the client UI  
http://localhost:8082  
*Note that this is not a very likely usecase; The client will probably be ran standalone, and possibly on a different host.*


**Front-end: Dockerized client in production mode**

1. In a console/terminal tab, cd to the project root (/microshopcustomerservice)

2. Cd into the client folder  
$ cd client

3. Build the Docker image  
$ docker build -t customer_service-client:1.0.0 .

4. Run the Docker image  
$ docker run -it --rm -p 8082:80 customer_service-client:1.0.0  
    *Note: if the API URL differs from http://localhost:8081/api, add an environment variable named VUE_APP_API_BASE_URL to the docker run command. For example:*  
    $ docker run -it --rm -p 8082:80 -e VUE_APP_API_BASE_URL=http://my-api-host:1234/api/v2 customer_service-client:1.0.0

5. Access the following URL in your browser  
http://localhost:8082

**Front-end: Dockerized client in development mode**

1. In a console/terminal tab, cd to the project root (/microshopcustomerservice)

2. Cd into the client folder  
$ cd client

3. Build the Docker image  
$ docker build -f Dockerfile-dev -t customer_service-client_dev:1.0.0 .

4. Run the Docker image  
$ docker run -it --rm -p 8082:8082 customer_service-client_dev:1.0.0  
    *Note: if the API URL differs from http://localhost:8082/api, add an environment variable named VUE_APP_API_BASE_URL to the docker run command. For example:*  
    $ docker run -it --rm -p 8082:8082 -e VUE_APP_API_BASE_URL=http://localhost:1234/api/v2 customer_service-client_dev:1.0.0
    
5. Access the following URL in your browser  
http://localhost:8082/


**Front-end: non-Dockerized client in development mode**

1. In a console/terminal tab, cd to the project root (/microshopcustomerservice)

2. Cd into the client folder  
$ cd client

3. Build and run the project from source  
$ npm run serve

4. Access the following URL in your browser  
http://localhost:8082


**Back-end: using Docker-compose**

1. Start Docker Desktop, which will start the Deamon

2. In a console/terminal tab, cd to the project root (/microshopcustomerservice)

3. Cd into app and build the project from source  
$ cd app  
$ mvn clean install

4. Build the Docker cluster using Docker-compose:  
$ docker-compose build

5. Run the Docker cluster:  
$ docker-compose up

6. Access the following URL in your browser / Postman. Note the different port, since the application is now served with NGINX.  
http://localhost:8081/api/customer/v1



**Back-end:  Dockerized app**

1. Start Docker Desktop, which will start the Deamon

2. In a console/terminal tab, create the Docker network (if you haven't done that already)  
$ docker network create customerservice_network

3. Start MongoDB (using the 'customerservice_network' network):  
$ docker run \  
  -it --rm \  
  -v ~/dev/mnt/mongodb-microshop-customer:/data/db \  
  -v ~/dev/mnt/mongodb-microshop-customer-config:/data/configdb \  
  -p 27017:27017 \  
  --network customerservice_network \  
  --name customerservice-mongodb \  
  mongo

4. In a new console/terminal tab, cd to the app root

5. Build the project from source  
$ mvn clean package  
*make sure the 'spring.data.mongodb.host=customerservice-mongodb' property is specified in application.properties*

6. Build the Docker image  
$ docker build -t customer_service:1.0.0 .

7. Run the Docker image  
$ docker run \  
  -it --rm \  
  -p 8080:8080 \  
  --network customerservice_network \  
  customer_service:1.0.0

8. Access the following URL in your browser / Postman:  
http://localhost:8080/api/customer/v1


**Back-end: non-Dockerized app**

1. Start Docker Desktop, which will start the Deamon

2. In a console/terminal tab, start MongoDB:  
$ docker run \  
  -it --rm \  
  -v ~/dev/mnt/mongodb-microshop-customer:/data/db \  
  -v ~/dev/mnt/mongodb-microshop-customer-config:/data/configdb \  
  -p 27017:27017 \  
  --name mongodb-microshop-customer \  
  mongo

3. In a new console/terminal tab, cd to the app root

4. Build the project from source  
$ mvn clean install  
*make sure the 'spring.data.mongodb.host' property is NOT specified in application.properties*

5. Run the app  
$ mvn spring-boot:run

6. Access the following URL in your browser / Postman:  
http://localhost:8080/api/customer/v1



## API ##

Root URL: /api/customer/v1


**Retrieve all customers**  

- Request:
  ```
  GET {ROOT}
  ```

- Request parameters:  
    - search (text, optional): Find customers by email (exact) or lastname (containing)
    - page (number, optional, default 1): Return only a specific page
    - size (number, optional, default 5): Specify the size of the page
  
- Response:
  ``` 
  OK (200)
  {
    "content": [
      {
          "id": "616d6d8bafdd48749c0b75f6",
          "firstname": "Abby",
          "lastname": "Aelstrom",
          "email": "abby@aelstrom.com",
          "password": null,
          "createdDate": "2021-10-18T12:50:19.934+00:00",
          "lastModifiedDate": "2021-10-18T12:50:19.934+00:00",
          "version": 0,
          "fullname": "Aelstrom, Abby"
      },
      [..]
    ],
    "meta": {
      "current": 1,
      "items": 6,
      "pages": 1
    }
  }
  ```
  
- Example:
  ```
  GET http://localhost/api/customer/v1?search=strom&page=1&size=5
  ```

  
**Retrieve a customer by ID**

- Request:
  ```
  GET {ROOT}/:id
  ```

- URL parameters:  
    - id (text, required)
  
- Response:
  ```
  OK (200)
  {
    "id": "616d6d8bafdd48749c0b75fb",
    "firstname": "Freddy",
    "lastname": "Freeloader",
    "email": "freddy@freeloader.com",
    "password": null,
    "createdDate": "2021-10-18T12:50:19.967+00:00",
    "lastModifiedDate": "2021-10-18T12:50:19.967+00:00",
    "version": 0,
    "fullname": "Freeloader, Freddy"
  }
  ```
  *or*
  ```
  NOT_FOUND (404)
  ```

- Example:
  ```
  GET http://localhost/api/customer/v1/12345
  ```

  
**Create a new customer**

- Request:
  ```
  POST {ROOT}
  ```

- JSON body parameters:
    - firstname (text, required)
    - lastname (text, required)
    - email (text, required)  
  
- Response:
  ```
  CREATED (201)
  ```

- Example:
  ```
  POST http://localhost/api/customer/v1/
  {
    "firstname": "Gianna",
    "lastname": "Gerkin",
    "email": "gianna@gerkin.com"
  }
  ```

**Update the customer with ID :id**  

- Request:
  ```
  PUT {ROOT}/:id
  ```

- URL parameters:
    - id (text, required)

- JSON body parameters:
    - firstname (text, required)
    - lastname (text, required
    - email (text, required)
  
- Response:
  ```
  OK (200)
  ```

- Example:
  ```
  PUT http://localhost/api/customer/v1/12345
  {
    "firstname": "Gianna",
    "lastname": "Gerkin",
    "email": "gianna@gerkin.com"
  }
  ```
  
**Update the email of the customer with ID :id**  

- Request:
  ```
  PATCH {ROOT}/:id
  ```

- URL parameters:
    - id (text, required)

- JSON body parameters:
    - email (text, required)
  
- Response:
  ```
  OK (200)
  ```
  
- Example:
  ```
  PATCH http://localhost/api/customer/v1/12345
  albert65aa@aelstrom.com
  ```
  

**Delete the customer with ID :id**  

- Request:
  ```
  DELETE  {ROOT}/:id
  ```

- URL parameters:
    - id (text, required)

- Response:
  ```
  NO_CONTENT (204)
  ```
  
- Example:
  ```
  DELETE http://localhost/api/customer/v1/12345
  ```



### Bulk operations

**Bulk-create customers**

- Request:
  ```
  POST {ROOT}/bulk
  ```

- JSON body parameters:  
  A list of:

    - firstname (text, required)
    - lastname (text, required)
    - email (text, required)
  
- Response:
  ```
  ACCEPTED (202)  
  ```

- Example:
  ```
  POST http://localhost/api/customer/v1/bulk
  [
    {
        "firstname": "Abby",
        "lastname": "Aelstrom",
        "email": "abby@aelstrom.com"
    },
    {
        "firstname": "Bento",
        "lastname": "Box",
        "email": "bento@box.com"
    }
  ]
  ```

**Bulk-delete customers**  

- Request:
  ```
  DELETE {ROOT}/bulk
  ```

- JSON body parameters:  
  A list of IDs.  

- Response:
  ```
  ACCEPTED (202)
  ```

- Example:
  ```
  DELETE http://localhost/api/customer/v1/bulk
  [
      "616d697b042e6a67b65e8ab9",
      "616d697b042e6a67b65e8aba",
      "616d697b042e6a67b65e8abb"
  ]
  ```

**Delete ALL customers**  

- Request:
  ```
  DELETE {ROOT}
  ```

- Response:
  ```
  ACCEPTED (202)
  ```

- Example:
  ```
  DELETE http://localhost/api/customer/v1/
  ```

---
*211208 Jasper Fontaine*