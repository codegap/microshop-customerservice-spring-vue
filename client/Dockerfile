# Docker file for an optimized production image for VueJS, served by NGINX on port 80
#
# Build:
#   $ docker build -t customer_service-client:1.0.0 .
#
# Run:
#   $ docker run -it --rm -p 8082:80 \
#       customer_service-client:1.0.0
#
# If the API URL differs from http://localhost:8081/api, we can add an environment variable named 
# VUE_APP_API_BASE_URL to the docker run command:
#   $ docker run -it --rm -p 8082:80 \
#     -e VUE_APP_API_BASE_URL=http://my-api-host:1234/api \
#     customer_service-client:1.0.0


########
#### Setup Stage
##

FROM node:lts-alpine as setup-stage

# make the 'app' folder the current working directory
WORKDIR /app

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .


########
#### Build Stage
##

FROM setup-stage AS build-stage

# run the production build
RUN npm run build


########
#### Production Stage
##

FROM nginx:alpine as production-stage

# Expose port 80
EXPOSE 80

# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

# Copy static assets from builder stage
COPY --from=build-stage /app/dist .


# Copy the entrypoint script that will inject the runtime environment 
# variables, then run it as entrypoint instead of CMD
COPY ./docker-entrypoint.custom.sh /docker-entrypoint.custom.sh
RUN chmod +x /docker-entrypoint.custom.sh

ENTRYPOINT ["/docker-entrypoint.custom.sh"]

