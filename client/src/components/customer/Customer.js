import Entity from "@/util/Entity";
import { required, email, minLength, maxLength } from "@vuelidate/validators";

class Customer extends Entity {
  fields = {
    firstname: {
      label: "Firstname",
      initialValue: "",
      validations: {
        required,
        minLength: minLength(1),
        maxLength: maxLength(50),
      }
    },
    lastname: {
      label: "Lastname",
      initialValue: "",
      validations: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(50),
      }
    },
    email: {
      label: "Email",
      initialValue: "",
      validations: {
        required,
        email,
      }
    }
  }
}

export default new Customer();
