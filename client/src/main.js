import 'bootstrap/dist/css/bootstrap.min.css'
import '@oruga-ui/oruga-next/dist/oruga-full.css'

import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router'

import 'bootstrap'
import { Notification } from '@oruga-ui/oruga-next'
import FontAwesomeIcon from '@/util/Icons';

createApp(App)
  .component('font-awesome-icon', FontAwesomeIcon)
  .use(router)
  .use(Notification)
  .mount('#app');