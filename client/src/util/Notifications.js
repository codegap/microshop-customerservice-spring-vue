import { useProgrammatic } from '@oruga-ui/oruga-next';

export function alert(msg) {
  useProgrammatic().oruga.notification.open({
    message: msg,
    duration: 5000,
    position: 'top',
    variant: 'danger',
    queue: true,
    rootClass: 'notification-alert',
  });
}
