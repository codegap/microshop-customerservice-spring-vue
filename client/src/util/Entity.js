/* 
  Baseclass for domain/entity definitions.

  Usage: extend, then define a config object in the child. Example:

  import Entity from "@/util/Entity";
  import { required, email, minLength, maxLength } from "@vuelidate/validators";

  class Customer extends Entity {
    fields: {
      firstname: {
        label: "Firstname",
        initialValue: "",
        validations: {
          required,
          minLength: minLength(1),
        }
      },
      ..

  When adding multiple entities to the same component, overwrite the entityName with a unique key like 'customer'.
  Some refactoring needs to be done on the State object though: prefix the error keys with the entity name.
*/
export default class Fields {
  entityName = "entity";

    //  entity: {
    //    firstname: "",
    //    config: {
    //      firstname: {
    //        label: "Firstname"
    //      }
    //    }
    //  }
  getDataConfig(addId = false) {
    let data = {};
    let config = [];
    for (let [k, v] of Object.entries(this.fields)) {
      data[k] = v.initialValue;
      config.push({ field: k, label: v.label });
    }
    if (addId) {
      data.id = ""
    }
    data.config = config;
    return { [this.entityName]: data };
  }

  getValidationsConfig() {
    let data = {};
    for (let [k, v] of Object.entries(this.fields)) {
      data[k] = v.validations;
    }
    return { [this.entityName] : data };
  }

  getData(component, getId = false) {
    let data = {};
    for (let k of Object.keys(this.fields)) {
      data[k] = component[this.entityName][k];
    }
    if (getId) {
      data.id = component[this.entityName].id;
    }
    return data;
  }

  setData(component, values, setId = false) {
    for (let k of Object.keys(this.fields)) {
      component[this.entityName][k] = values[k];
    }
    if (setId) {
      component[this.entityName].id = values.id;
    }
  }

  resetData(component) {
    for (let [k, v] of Object.entries(this.fields)) {
      component.entity[k] = v.initialValue;
    }
  }
}
