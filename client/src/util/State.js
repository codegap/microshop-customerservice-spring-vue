import { arrayToMap } from "@/util/Collections";

export default class State {
  constructor() {
    this.errors = {};
  }

  setErrorsFromVuelidate(v) {
    this.errors = arrayToMap(v.$errors, '$property', '$message') || {};
    console.log("errors: ",this.errors);
  }

  setErrorsFromServer(e) {
    this.errors = e.response.data.errors || {};
    console.log("errors: ",this.errors);
  }

  resetErrors() {
    this.errors = {};
  }
}