/*
Install:
  npm i --save @fortawesome/fontawesome-svg-core
  npm i --save @fortawesome/free-solid-svg-icons
  npm i --save @fortawesome/free-regular-svg-icons
  npm i --save @fortawesome/vue-fontawesome@prerelease

Usage in component template:
  <!-- explicit style -->
  <font-awesome-icon :icon="['fas', 'cog']" />

  <!-- implicit style (fas is assumed) -->
  <font-awesome-icon icon="cog" />
*/

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';

import {
  faArrowAltCircleLeft,
  faBan,
  faCog, 
  faEdit, 
  faList,
  faPlusSquare, 
  faSave, 
  faTimes, 
  faTrashAlt,
  faUser, 
} from '@fortawesome/free-solid-svg-icons';

library.add(  
  faArrowAltCircleLeft,
  faBan,
  faCog, 
  faEdit, 
  faList,
  faPlusSquare, 
  faSave, 
  faTimes, 
  faTrashAlt,
  faUser, 
);

export default FontAwesomeIcon;