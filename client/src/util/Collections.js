/* 
  array:
    [
      {field: "email", message: "must not be blank"},
      {field: "firstname", message: "must not be blank"},
      {field: "firstname", message: "size must be between 1 and 50"}
    ]
  call:
    arrayToMap(arr, 'field', 'message');
  output:
    {
      email: ['must not be blank'],
      firstname: ['must not be blank', 'size must be between 1 and 50']
    }
*/
export function arrayToMap(objArr, key = 'key', value = 'value') {
  return objArr.reduce((map, e) => {
    map[e[key]] = map[e[key]] || [];
    map[e[key]].push(e[value]);
    return map;
  }, {});
}