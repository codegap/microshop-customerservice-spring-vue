import axios from 'axios';
import { apiUrl } from '../app.config';
import { alert } from '@/util/Notifications';
import { arrayToMap } from "@/util/Collections";

// Axios instantiation
const request = axios.create({
  baseURL: apiUrl,
  headers: {
    "Content-type": "application/json"
  }
});

// request interceptor
request.interceptors.request.use(req => {
  console.log(`${req.method.toUpperCase()} ${req.url}`);
  return req;
});

// response interceptor
request.interceptors.response.use(
  res => res, // pass-through - no action on success response
  err => errorhandler(err) // handle the error
);

// handling the error: make sure data.message has a value, then
// either show a global alert or prepare the validation errors.
// Then return a rejected Promise to the calling client 
const errorhandler = (err) => {
  const data = err.response.data;
  data.message = data.message || err.message;
  console.log("Error response: ", data.message);

  if (data.fieldErrors) {
    // had validation errors. Convert to common format and add to error object
    data.errors = arrayToMap(data.fieldErrors, 'field', 'message')
    console.log("  └ fields: ", data.errors);
  } else {
    // global error, show alert.
    // note: can be triggered by accessing /customer/edit/error
    alert(data.message);
  }

  return Promise.reject(err);
}

// export
export default request;