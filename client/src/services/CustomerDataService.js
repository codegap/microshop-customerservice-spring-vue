import request from '@/http-common';

class CustomerDataService {
  // params: search, page, size - all optional
  async getAll(params) {
    return request.get("/customer/v1", { params });
  }

  async get(id) {
    return request.get(`/customer/v1/${id}`);
  }

  async create(data) {
    return request.post("/customer/v1", data);
  }

  async update(id, data) {
    return request.put(`/customer/v1/${id}`, data);
  }

  async remove(id) {
    return request.delete(`/customer/v1/${id}`);
  }
}

export default new CustomerDataService();