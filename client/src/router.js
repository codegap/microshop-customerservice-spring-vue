import { createWebHistory, createRouter } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'customer-list',
    component: () => import('./components/customer/List')
  },
  {
    path: '/customer/edit/:id',
    name: 'customer-edit',
    component: () => import('./components/customer/Edit')
  },
  {
    path: '/customer/add',
    name: 'customer-add',
    component: () => import('./components/customer/Add')
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;