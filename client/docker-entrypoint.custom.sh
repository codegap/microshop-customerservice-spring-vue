#!/bin/sh

ROOT_DIR=/usr/share/nginx/html

# set the API URL from the VUE_APP_API_BASE_URL environment variable, or 
# use the default localhost URL if the environment variable is not set.
API_URL=${VUE_APP_API_BASE_URL:=http://localhost:8081/api}

# Replace env vars in JavaScript files
echo "/docker-entrypoint.custom.sh: replacing constants in JS"
echo "/docker-entrypoint.custom.sh:   'PLACEHOLDER_VUE_APP_API_BASE_URL' -> '$API_URL'"
for file in $ROOT_DIR/js/*.js*
do
  sed -i 's|'PLACEHOLDER_VUE_APP_API_BASE_URL'|'$API_URL'|g' $file
done

echo "/docker-entrypoint.custom.sh: starting NGINX"
nginx -g 'daemon off;'