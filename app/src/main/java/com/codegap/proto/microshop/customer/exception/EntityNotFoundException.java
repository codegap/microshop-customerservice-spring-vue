package com.codegap.proto.microshop.customer.exception;

import lombok.Getter;

public class EntityNotFoundException extends RuntimeException {
    @Getter
    private final String id;

    public EntityNotFoundException(String id) {
        super("Entity not found for id '%s'".formatted(id));
        this.id = id;
    }
}
