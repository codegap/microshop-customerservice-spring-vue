package com.codegap.proto.microshop.customer.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PagedListDto<T> {
    @Data
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Meta {
        long items = 0;
        int pages = 0;
        int current = 1;
    }

    private Meta meta;
    private List<T> content;

    public static <T> PagedListDto<T> of(Page<T> page) {
        if (page == null) {
            return new PagedListDto<>(new Meta(0, 0, 1), List.of());
        }

        return new PagedListDto<T>(
                new Meta(
                        page.getTotalElements(),
                        page.getTotalPages(),
                        page.getNumber() + 1 // page-number is 1-based in the UI
                ),
                page.getContent()
        );
    }
}
