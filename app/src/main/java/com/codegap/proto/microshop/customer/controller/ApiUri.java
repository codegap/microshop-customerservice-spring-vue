package com.codegap.proto.microshop.customer.controller;

import com.codegap.proto.microshop.customer.model.PersistableEntity;

public final class ApiUri {
    static final class PathVariable {
        public static final String ID = "id";
    }

    public static final String API_PREFIX = "/api";

    public static final String ID = "/{" + PathVariable.ID + "}";
    public static final String BATCH = "/batch";
    public static final String ERROR = "/error"; // for health monitoring & testing

    public static final String CUSTOMER_V1 = API_PREFIX + "/customer/v1";


    public static String getIdentityUri(String collectionUri, PersistableEntity<?> t) {
        return getIdentityUri(collectionUri, t.getId());
    }

    public static String getIdentityUri(String collectionUri, String id) {
        return collectionUri + "/" + id;
    }

    public static String getBatchUri(String collectionUri) {
        return collectionUri + ApiUri.BATCH;
    }

    public static String getErrorUri(String collectionUri) {
        return collectionUri + ApiUri.ERROR;
    }
}
