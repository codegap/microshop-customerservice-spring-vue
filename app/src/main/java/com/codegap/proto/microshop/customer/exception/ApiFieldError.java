package com.codegap.proto.microshop.customer.exception;

import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.validation.FieldError;

import javax.validation.ConstraintViolation;

public record ApiFieldError(String field, String message) {

    public static ApiFieldError of(FieldError fieldError) {
        return new ApiFieldError(
                fieldError.getField(),
                fieldError.getDefaultMessage()
        );
    }

    public static ApiFieldError of(ConstraintViolation<?> constraintViolation) {
        return new ApiFieldError(
                ((PathImpl)constraintViolation.getPropertyPath()).getLeafNode().getName(),
                constraintViolation.getMessage()
        );
    }
}
