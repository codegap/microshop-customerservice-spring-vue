package com.codegap.proto.microshop.customer.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    @NonNull
    protected ResponseEntity<Object> handleHttpMessageNotReadable(@NonNull HttpMessageNotReadableException ex, @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
        return respond(new ApiError(HttpStatus.BAD_REQUEST, "Malformed JSON request", ex));
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleHttpMessageNotWritable(@NonNull HttpMessageNotWritableException ex, @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
        return respond(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Could not write JSON", ex));
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull MethodArgumentNotValidException ex, @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
        Optional<List<ApiFieldError>> fieldErrors =
                convertToApiFieldErrors(ex.getBindingResult().getFieldErrors(), ApiFieldError::of);
        return respond(new ApiError(HttpStatus.BAD_REQUEST, "Method argument not valid", ex, fieldErrors.orElse(null)));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
        Optional<List<ApiFieldError>> fieldErrors =
                convertToApiFieldErrors(ex.getConstraintViolations(), ApiFieldError::of);
        return respond(new ApiError(HttpStatus.BAD_REQUEST, "Method argument not valid", ex, fieldErrors.orElse(null)));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex) {
        return respond(new ApiError(HttpStatus.NOT_FOUND, "Entity not found for id '%s'".formatted(ex.getId())));
    }

    @ExceptionHandler(DataAccessException.class)
    protected ResponseEntity<Object> handleDataAccessException(DataAccessException ex) {
        return respond(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error accessing data", ex));
    }

    // catch-all
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> catchAll(Exception ex) {
        return respond(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex));
    }

    private ResponseEntity<Object> respond(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    private <T> Optional<List<ApiFieldError>> convertToApiFieldErrors(Collection<T> errors, Function<T, ApiFieldError> mapper) {
        if (errors != null && !errors.isEmpty()) {
            return Optional.of(errors.stream()
                    .map(mapper)
                    .sorted(Comparator.comparing(ApiFieldError::field).thenComparing(ApiFieldError::message))
                    .toList());
        }
        return Optional.empty();
    }
}

