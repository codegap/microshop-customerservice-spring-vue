package com.codegap.proto.microshop.customer.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@Data
@JsonInclude(Include.NON_NULL)
public class ApiError {
    private final LocalDateTime timestamp = LocalDateTime.now();
    private HttpStatus status;
    private String message;
    private List<ApiFieldError> fieldErrors;
    private String debugMessage;

    ApiError(HttpStatus status, Throwable ex) {
        this(status, "Unexpected error", ex);
    }

    ApiError(HttpStatus status, String message) {
        this(status, message, (String)null, null);
    }

    ApiError(HttpStatus status, String message, Throwable ex) {
        this(status, message, ex, null);
    }

    ApiError(HttpStatus status, String message, Throwable throwable, List<ApiFieldError> fieldErrors) {
        this(status, message, Optional.ofNullable(throwable).map(Throwable::toString).orElse(null), fieldErrors);
    }

    ApiError(HttpStatus status, String message, String debugMessage, List<ApiFieldError> fieldErrors) {
        this.status = status;
        this.message = message;
        this.debugMessage = debugMessage;
        this.fieldErrors = fieldErrors;
    }
}
