package com.codegap.proto.microshop.customer.controller.impl;

import com.codegap.proto.microshop.customer.controller.AbstractPagingCrudController;
import com.codegap.proto.microshop.customer.controller.ApiUri;
import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.service.impl.CustomerService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        value = ApiUri.CUSTOMER_V1,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class CustomerController extends AbstractPagingCrudController<Customer> {
    public CustomerController(CustomerService customerService, CustomerFinderResolver customerFinderResolver) {
        super(customerService, customerFinderResolver);
    }

    // more Customer-specific / non-CRUD endpoints here..
}
