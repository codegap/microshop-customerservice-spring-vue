package com.codegap.proto.microshop.customer.controller;

import com.codegap.proto.microshop.customer.dto.PagedListDto;
import com.codegap.proto.microshop.customer.model.PersistableEntity;
import com.codegap.proto.microshop.customer.service.PagingCrudService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.function.Supplier;

@RequiredArgsConstructor
@Validated
public abstract class AbstractPagingCrudController<T extends PersistableEntity<T>> extends AbstractController<T> {
    protected final PagingCrudService<T> service;
    protected final FinderResolver<T> finderResolver;

    public static final String DEFAULT_PAGE = "1";
    public static final String DEFAULT_PAGESIZE = "5";

    @GetMapping
    public ResponseEntity<PagedListDto<T>> findAll(
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = DEFAULT_PAGE) int page,
            @RequestParam(defaultValue = DEFAULT_PAGESIZE) int size) {
        Supplier<Page<T>> finderSupplier = finderResolver.resolve(search, toPageable(page, size));
        Page<T> result = finderSupplier.get();
        return ResponseEntity.ok(PagedListDto.of(result));
    }

    @GetMapping(path = ApiUri.ID)
    public ResponseEntity<T> getById(@PathVariable(ApiUri.PathVariable.ID) String id) {
        return ok(service.getById(id)); // throws EntityNotFoundException if not found
    }

    @PostMapping
    public ResponseEntity<Void> create(@Valid @RequestBody T t) {
        return created(service.insert(t).getId());
    }

    @PutMapping(path = ApiUri.ID)
    public ResponseEntity<T> update(@PathVariable(ApiUri.PathVariable.ID) String id, @Valid @RequestBody T t) {
        return ok(service.update(t.withId(id)));
    }

    @DeleteMapping(path = ApiUri.ID)
    public ResponseEntity<Void> delete(@PathVariable(ApiUri.PathVariable.ID) String id) {
        service.delete(id);
        return noContent();
    }

    @DeleteMapping // no postfix; operating on the whole collection
    public ResponseEntity<Void> deleteAll() {
        service.deleteAll();
        return accepted();
    }

    // Batch endpoints for testing only - hence the quick 'n dirty, non-informative responses
    @PostMapping(path = ApiUri.BATCH)
    public ResponseEntity<Void> createBatch(@RequestBody List<@Valid T> list) {
        service.insertAll(list);
        return created();
    }

    @DeleteMapping(path = ApiUri.BATCH)
    public ResponseEntity<Void> deleteBatch(@RequestBody List<@NotBlank String> ids) {
        service.deleteAll(ids);
        return accepted();
    }

    @GetMapping(path = ApiUri.ERROR)
    public ResponseEntity<Void> error() {
        service.throwDataAccessException();
        return noContent(); // will never happen
    }
}
