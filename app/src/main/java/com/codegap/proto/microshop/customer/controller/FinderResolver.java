package com.codegap.proto.microshop.customer.controller;

import io.github.lprakashv.patternmatcher4j.exceptions.PMatcherException;
import io.github.lprakashv.patternmatcher4j.matcher.PMatcher;
import lombok.extern.flogger.Flogger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.function.Supplier;

@Flogger
public abstract class FinderResolver<T> {

    public Supplier<Page<T>> resolve(String search, Pageable pageable) {
        try {
            return createAndExecuteSearchTermMatcher(search, pageable);
        } catch (PMatcherException e) {
            log.atWarning().withCause(e).log("Error trying to resolve searchcommand for '%s'", search);
            return getEmptyResultSupplier();
        }
    }

    public Supplier<Page<T>> createAndExecuteSearchTermMatcher(String search, Pageable pageable) throws PMatcherException {
        PMatcher<String, Supplier<Page<T>>> matcher = new PMatcher<>(search);
        configureMatches(matcher, search, pageable);
        return matcher.getOrElse(getDefaultSupplier(search, pageable));
    }

    protected Supplier<Page<T>> getEmptyResultSupplier() {
        return () -> new PageImpl<>(List.of());
    }

    protected abstract void configureMatches(PMatcher<String, Supplier<Page<T>>> matcher, String search, Pageable pageable);
    protected abstract Supplier<Page<T>> getDefaultSupplier(String search, Pageable pageable);
}