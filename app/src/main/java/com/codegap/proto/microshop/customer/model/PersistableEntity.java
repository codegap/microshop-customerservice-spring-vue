package com.codegap.proto.microshop.customer.model;


public abstract class PersistableEntity<T extends PersistableEntity<T>> {
    public abstract String getId();

    public abstract T setId(String id);

    public abstract T copy();

    public abstract T merge(T merge);

    public boolean isNew() {
        return (getId() == null);
    }

    // clears the id of this entity, then returns it
    public T asNew() {
        return setId(null);
    }

    // copies this entity, then clears its id and returns it
    public T copyAsNew() {
        return copy().setId(null);
    }

    // overwrites the id of this entity, then returns it
    public T withId(String id) {
        return setId(id);
    }

    // copies this entity, then overwrites its id and returns it
    public T copyWithId(String id) {
        return copy().setId(id);
    }
}
