package com.codegap.proto.microshop.customer.controller;

import com.codegap.proto.microshop.customer.model.Customer;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public abstract class AbstractController<T> {
    protected static Pageable toPageable(int page, int size) {
        page = Math.max(page, 1) - 1;  // page-number is 1-based in the UI
        size = Math.max(size, 1);
        Sort sort = Customer.SORT_DEFAULT;
        return PageRequest.of(page, size, sort);
    }

    protected ResponseEntity<T> ok(T t) { return ResponseEntity.ok(t); }
    protected ResponseEntity<Void> created() { return ResponseEntity.status(HttpStatus.CREATED).build(); }
    protected ResponseEntity<Void> created(String id) { return created("/{id}", id); }
    protected ResponseEntity<Void> created(String path, String id) {
        return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().path(path).buildAndExpand(id).toUri()).build();
    }
    protected ResponseEntity<T> notFound() { return ResponseEntity.notFound().build(); }
    protected ResponseEntity<Void> noContent() { return ResponseEntity.noContent().build(); }
    protected ResponseEntity<Void> accepted() { return ResponseEntity.accepted().build(); }
}
