package com.codegap.proto.microshop.customer.service;

import com.codegap.proto.microshop.customer.exception.EntityNotFoundException;
import com.codegap.proto.microshop.customer.model.PersistableEntity;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * All methods can throw (a subclass of) Spring's DataAccessException.
 */
public abstract class PagingCrudService<T extends PersistableEntity<T>> {

    protected abstract PagingAndSortingRepository<T, String> getRepository();

    public Page<T> findAll(@NotNull Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    /**
     * Throws EntityNotFoundException if not found
     */
    public T getById(@NotBlank String id) {
        return getRepository().findById(id)
                .orElseThrow(() -> new EntityNotFoundException(id));
    }

    public T insert(@Valid T entity) {
        return getRepository().save(entity.asNew()); // making sure it is inserted as a new record
    }

    @Transactional // all or nothing. For testing only
    public void insertAll(List<@Valid T> list) {
        list.parallelStream().forEach(this::insert);
    }

    /**
     * Throws EntityNotFoundException if not found
     *
     * @param entity the entity to merge with the persisted entity with the same id
     * @return the updated entity
     */
    @Transactional
    public T update(@Valid T entity) {
        return getRepository().findById(entity.getId())
                .map(e -> persistUpdate(e, entity))
                .orElseThrow(() -> new EntityNotFoundException(entity.getId()));
    }

    protected T persistUpdate(T existing, T entity) {
        T merged = existing.merge(entity);
        return getRepository().save(merged);
    }

    public void delete(@NotBlank String id) {
        getRepository().deleteById(id);
    }

    @Transactional // all or nothing. For testing only
    public void deleteAll(@NotEmpty List<@NotBlank String> ids) {
        ids.parallelStream().forEach(this::delete);
    }

    public void deleteAll() {
        getRepository().deleteAll();
    }

    public void throwDataAccessException() {
        throw new DataIntegrityViolationException("DataAccessException for health monitoring & testing purposes");
    }
}
