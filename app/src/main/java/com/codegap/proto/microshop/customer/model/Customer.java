package com.codegap.proto.microshop.customer.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Document(collection = "Customer")
@Getter
@Setter
@Builder(toBuilder = true)
@Accessors(chain = true)
@ToString
public class Customer extends PersistableEntity<Customer> {
    public static final Sort SORT_DEFAULT = Sort.by(
            new Sort.Order(Sort.Direction.ASC, "lastname"),
            new Sort.Order(Sort.Direction.ASC, "firstname")
    );

    @Id
    private String id;

    @NotBlank
    @Size(min=1, max=50)
    private String firstname;

    @NotBlank
    @Size(min=2, max=50)
    private String lastname;

    @Email
    @NotBlank
    private String email;

    private String password;

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @Version
    private Long version;

    public String getFullname() {
        return String.format("%s, %s", lastname, firstname);
    }

    // Returns a copy of this entity after overwriting the firstname,
    // lastname and email fields with the values from the supplied object
    @Override
    public Customer merge(Customer merge) {
        return this.toBuilder()
                .firstname(merge.getFirstname())
                .lastname(merge.getLastname())
                .email(merge.getEmail())
                .build();
    }

    @Override
    public Customer copy() {
        return this.toBuilder().build();
    }

    public static Customer of(String id, String firstname, String lastname, String email) {
        return of(id, firstname, lastname, email, null);
    }

    public static Customer of(String id, String firstname, String lastname, String email, String password) {
        return Customer.builder()
                .id(id)
                .firstname(firstname)
                .lastname(lastname)
                .email(email)
                .password(password)
                .build();
    }
}
