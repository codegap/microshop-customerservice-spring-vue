package com.codegap.proto.microshop.customer.service.impl;

import com.codegap.proto.microshop.customer.dao.CustomerRepository;
import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.service.PagingCrudService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Service
@Validated
@RequiredArgsConstructor
public class CustomerService extends PagingCrudService<Customer> {
    @Getter
    final CustomerRepository repository;

    public Page<Customer> findAllByEmailContaining(@NotBlank String email, @NotNull Pageable pageable) {
        return repository.findAllByEmailContaining(email, pageable);
    }

    public Page<Customer> findAllByLastnameContaining(@NotBlank String lastname, @NotNull Pageable pageable) {
        return repository.findAllByLastnameContaining(lastname, pageable);
    }
}
