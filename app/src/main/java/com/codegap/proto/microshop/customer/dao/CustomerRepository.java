package com.codegap.proto.microshop.customer.dao;

import com.codegap.proto.microshop.customer.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {
    Page<Customer> findAllByEmailContaining(String email, Pageable pageable);
    Page<Customer> findAllByLastnameContaining(String partialLastname, Pageable pageable);
}
