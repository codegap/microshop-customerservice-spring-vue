package com.codegap.proto.microshop.customer.controller.impl;

import com.codegap.proto.microshop.customer.controller.FinderResolver;
import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.service.impl.CustomerService;
import io.github.lprakashv.patternmatcher4j.matcher.PMatcher;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;
import java.util.function.Supplier;

@Component
@RequiredArgsConstructor
public class CustomerFinderResolver extends FinderResolver<Customer> {
    private final CustomerService customerService;

    private static final Predicate<Object> hasSearchTerm = s -> s instanceof String ss && !ss.isBlank();
    private static final Predicate<Object> matchesEmail = s -> ((String) s).contains("@"); // very naive implementation, but that's not the point

    @Override
    protected void configureMatches(PMatcher<String, Supplier<Page<Customer>>> matcher, String search, Pageable pageable) {
        matcher.matchCase(hasSearchTerm.and(matchesEmail))
                .thenReturn(() -> customerService.findAllByEmailContaining(search, pageable));
        matcher.matchCase(hasSearchTerm)
                .thenReturn(() -> customerService.findAllByLastnameContaining(search, pageable));
    }

    @Override
    protected Supplier<Page<Customer>> getDefaultSupplier(String search, Pageable pageable) {
        return () -> customerService.findAll(pageable);
    }
}
