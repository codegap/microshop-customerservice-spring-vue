package com.codegap.proto.microshop.customer.model;

public record ConstraintViolatingEntity<T extends PersistableEntity<T>>(String property, String message, T entity) {
}
