package com.codegap.proto.microshop.customer.controller.impl;

import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.service.impl.CustomerService;
import io.github.lprakashv.patternmatcher4j.exceptions.PMatcherException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class CustomerFinderResolverTest {
    private static final Pageable DEFAULT_PAGEABLE = PageRequest.of(1, 20, Customer.SORT_DEFAULT);
    private static final Page<Customer> DEFAULT_RESULT = new PageImpl<>(List.of());

    @Mock
    CustomerService customerService;

    @InjectMocks
    CustomerFinderResolver customerFinderResolver;

    @Test
    void testResolveWithEmailSearch() {
        final String SEARCH = "foo@bar.com";

        when(customerService.findAllByEmailContaining(eq(SEARCH), eq(DEFAULT_PAGEABLE))).thenReturn(DEFAULT_RESULT);

        Supplier<Page<Customer>> supplier = customerFinderResolver.resolve(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(supplier);

        Page<Customer> result = supplier.get();

        assertEquals(DEFAULT_RESULT, result);
        verify(customerService, times(1)).findAllByEmailContaining(eq(SEARCH), eq(DEFAULT_PAGEABLE));
    }

    @Test
    void testResolveWithLastnameSearch() {
        final String SEARCH = "myPartialLastnam";

        when(customerService.findAllByLastnameContaining(eq(SEARCH), eq(DEFAULT_PAGEABLE))).thenReturn(DEFAULT_RESULT);

        Supplier<Page<Customer>> supplier = customerFinderResolver.resolve(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(supplier);

        Page<Customer> result = supplier.get();

        assertEquals(DEFAULT_RESULT, result);
        verify(customerService, times(1)).findAllByLastnameContaining(eq(SEARCH), eq(DEFAULT_PAGEABLE));
    }

    @Test
    void testResolveWithBlankSearch() {
        final String SEARCH = "  ";

        when(customerService.findAll(eq(DEFAULT_PAGEABLE))).thenReturn(DEFAULT_RESULT);

        Supplier<Page<Customer>> supplier = customerFinderResolver.resolve(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(supplier);

        Page<Customer> result = supplier.get();

        assertEquals(DEFAULT_RESULT, result);
        verify(customerService, times(1)).findAll(eq(DEFAULT_PAGEABLE));
    }

    @Test
    void testResolveWithNullSearch() {
        when(customerService.findAll(eq(DEFAULT_PAGEABLE))).thenReturn(DEFAULT_RESULT);

        Supplier<Page<Customer>> supplier = customerFinderResolver.resolve(null, DEFAULT_PAGEABLE);

        assertNotNull(supplier);

        Page<Customer> result = supplier.get();

        assertEquals(DEFAULT_RESULT, result);
        verify(customerService, times(1)).findAll(eq(DEFAULT_PAGEABLE));
    }

    @Test
    void testResolveWithNullSearchAndPageable() {
        when(customerService.findAll(isNull())).thenReturn(DEFAULT_RESULT);

        Supplier<Page<Customer>> supplier = customerFinderResolver.resolve(null, null);

        assertNotNull(supplier);

        Page<Customer> result = supplier.get();

        assertEquals(DEFAULT_RESULT, result);
        verify(customerService, times(1)).findAll(isNull());
    }

    @Test
    void testResolveWithExceptionExpectingEmptyResult() throws PMatcherException {
        final String SEARCH = "SEARCH";

        CustomerFinderResolver spiedResolver = Mockito.spy(customerFinderResolver);
        when(spiedResolver.createAndExecuteSearchTermMatcher(eq(SEARCH), eq(DEFAULT_PAGEABLE))).thenThrow(
                new PMatcherException(0, SEARCH, new AssertionError("My Error Message"))
        );

        Supplier<Page<Customer>> supplier = spiedResolver.resolve(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(supplier);
        assertEquals(0, supplier.get().getTotalElements());
        verifyNoInteractions(customerService);
    }
}