package com.codegap.proto.microshop.customer.service.impl;

import com.codegap.proto.microshop.customer.dao.CustomerRepository;
import com.codegap.proto.microshop.customer.model.ConstraintViolatingEntity;
import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.service.CrudServiceTestConfig;
import com.codegap.proto.microshop.customer.service.PagingCrudServiceTest;
import lombok.Getter;
import lombok.extern.flogger.Flogger;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@SpringBootTest
@ContextConfiguration(classes = {CustomerService.class, CustomerServiceTest.TestConfig.class, ValidationAutoConfiguration.class})
@Flogger
public class CustomerServiceTest extends PagingCrudServiceTest<Customer> {

    @MockBean
    @Getter
    private CustomerRepository repository;

    @Autowired
    @Getter
    private CustomerService service;

    @TestConfiguration
    static class TestConfig {
        @Bean
        public CrudServiceTestConfig<Customer> config() {
            return new CrudServiceTestConfig<Customer>()
                    .withEntitySupplier(() -> List.of(
                            Customer.of("1", "Barney", "Box", "barney@box.com", "BARNEY"),
                            Customer.of("2", "Courtney", "Cox", "courtney@cox.com", "COURTNEY"),
                            Customer.of("3", "Eddy", "Eagle", "eddy@eagle.com", "EDDY")
                    ))
                    .withConstraintViolatingEntitySupplier(() ->
                            new ConstraintViolatingEntity<>("entity.firstname", "must not be blank", Customer.of("1", null, "Box", "barney@box.com", "BARNEY"))
                    ).withEntityVerifier((expected, actual) -> {
                        assertEquals(expected.getId(), actual.getId());
                        assertEquals(expected.getEmail(), actual.getEmail());
                        assertEquals(expected.getLastname(), actual.getLastname());
                    })
                    .withUpdateTransformer(c ->
                            c.toBuilder().firstname("F_CHANGED").lastname("L_CHANGED").build()
                    );
        }
    }

    @Test
    void testFindAllByEmail() {
        final Customer ENTITY = getEntities().get(0);
        final String SEARCH = ENTITY.getEmail();

        when(repository.findAllByEmailContaining(eq(SEARCH), any(Pageable.class))).thenReturn(
                new PageImpl<>(List.of(ENTITY))
        );

        Page<Customer> result = service.findAllByEmailContaining(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(result);
        assertEquals(1, result.getSize());
        config.verifyEntity(ENTITY, result.getContent().get(0));

        verify(repository, times(1)).findAllByEmailContaining(eq(SEARCH), any(Pageable.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void testFindAllByEmailNotFound() {
        final String SEARCH = "XYZ";

        when(repository.findAllByEmailContaining(eq(SEARCH), any(Pageable.class))).thenReturn(
                Page.empty()
        );

        Page<Customer> result = service.findAllByEmailContaining(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(result);
        assertEquals(0, result.getSize());

        verify(repository, times(1)).findAllByEmailContaining(eq(SEARCH), any(Pageable.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void testFindAllByEmailConstraintViolationException() {
        assertConstraintViolationException("findAllByEmailContaining.email", "must not be blank", () ->
                service.findAllByEmailContaining(null, DEFAULT_PAGEABLE)
        );
        assertConstraintViolationException("findAllByEmailContaining.email", "must not be blank", () ->
                service.findAllByEmailContaining(" ", DEFAULT_PAGEABLE)
        );
    }

    @Test
    void testFindAllByLastnameContaining() {
        final String SEARCH = "ox";
        final Customer ENTITY_0 = getEntities().get(0);
        final Customer ENTITY_1 = getEntities().get(1);

        when(repository.findAllByLastnameContaining(eq(SEARCH), any(Pageable.class))).thenReturn(
                new PageImpl<>(List.of(ENTITY_0, ENTITY_1))
        );

        Page<Customer> result = service.findAllByLastnameContaining(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(result);
        assertEquals(2, result.getSize());
        List<Customer> list = result.getContent();
        config.verifyEntity(ENTITY_0, list.get(0));
        config.verifyEntity(ENTITY_1, list.get(1));

        verify(repository, times(1)).findAllByLastnameContaining(eq(SEARCH), any(Pageable.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void testFindAllByLastnameContainingNotFound() {
        final String SEARCH = "XYZ";

        when(repository.findAllByLastnameContaining(eq(SEARCH), any(Pageable.class))).thenReturn(
                Page.empty()
        );

        Page<Customer> result = service.findAllByLastnameContaining(SEARCH, DEFAULT_PAGEABLE);

        assertNotNull(result);
        assertEquals(0, result.getSize());

        verify(repository, times(1)).findAllByLastnameContaining(eq(SEARCH), any(Pageable.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void testFindAllByLastnameConstraintViolationException() {
        assertConstraintViolationException("findAllByLastnameContaining.lastname", "must not be blank", () ->
                service.findAllByLastnameContaining(null, DEFAULT_PAGEABLE)
        );
        assertConstraintViolationException("findAllByLastnameContaining.lastname", "must not be blank", () ->
                service.findAllByLastnameContaining(" ", DEFAULT_PAGEABLE)
        );
    }

    @Test
    void testUpdateExceptPassword() {
        final Customer ENTITY = getEntities().get(0);
        final String PASSWORD = "PASSWORD";
        final Customer UPDATE = ENTITY.toBuilder().password(PASSWORD).build();

        when(repository.findById(eq(UPDATE.getId()))).thenReturn(
                Optional.of(ENTITY)
        );
        when(repository.save(any())).then(
                AdditionalAnswers.returnsFirstArg()
        );

        Customer result = service.update(UPDATE);

        assertNotNull(result);
        config.verifyEntity(ENTITY, result);
        assertNotEquals(UPDATE.getPassword(), result.getPassword()); // not updated, even if changed in the input entity

        verify(repository, times(1)).findById(eq(UPDATE.getId()));
        verify(repository, times(1)).save(any());
        verifyNoMoreInteractions(repository);
    }
}
