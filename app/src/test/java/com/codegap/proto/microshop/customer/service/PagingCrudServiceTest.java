package com.codegap.proto.microshop.customer.service;

import com.codegap.proto.microshop.customer.exception.EntityNotFoundException;
import com.codegap.proto.microshop.customer.model.ConstraintViolatingEntity;
import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.model.PersistableEntity;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public abstract class PagingCrudServiceTest<T extends PersistableEntity<T>> {
    protected static final Pageable DEFAULT_PAGEABLE = PageRequest.of(1, 20, Customer.SORT_DEFAULT);

    protected abstract PagingAndSortingRepository<T, String> getRepository();

    protected abstract PagingCrudService<T> getService();

    @Autowired
    protected CrudServiceTestConfig<T> config;

    @Getter
    private List<T> entities;

    @BeforeEach
    void setup() {
        entities = config.createEntities();
    }

    @Test
    void testFindAllPaging() {
        when(getRepository().findAll(any(Pageable.class))).thenReturn(
                new PageImpl<>(entities)
        );

        Page<T> result = getService().findAll(DEFAULT_PAGEABLE);

        assertNotNull(result);
        assertEquals(3, result.getSize());

        verify(getRepository(), times(1)).findAll(any(Pageable.class));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    void testFindAllPagingConstraintViolationException() {
        assertConstraintViolationException("findAll.pageable", "must not be null", () ->
                getService().findAll(null)
        );
    }


    @Test
    void testGetById() {
        final T ENTITY = entities.get(0);
        final String SEARCH = ENTITY.getId();

        when(getRepository().findById(eq(SEARCH))).thenReturn(
                Optional.of(ENTITY)
        );

        T result = getService().getById(SEARCH);

        assertNotNull(result);
        config.verifyEntity(ENTITY, result);

        verify(getRepository(), times(1)).findById(eq(SEARCH));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    void testGetByIdNotFound() {
        final String UNKNOWN_ID = "UNKNOWN";

        when(getRepository().findById(eq(UNKNOWN_ID))).thenReturn(
                Optional.empty()
        );

        EntityNotFoundException enfe = assertThrows(EntityNotFoundException.class, () ->
                getService().getById(UNKNOWN_ID)
        );
        assertEquals(UNKNOWN_ID, enfe.getId());

        verify(getRepository(), times(1)).findById(eq(UNKNOWN_ID));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    void testGetByIdConstraintViolationException() {
        assertConstraintViolationException("getById.id", "must not be blank", () ->
                getService().getById(null)
        );
        assertConstraintViolationException("getById.id", "must not be blank", () ->
                getService().getById(" ")
        );
    }

    @Test
    void testInsert() {
        final T ENTITY = entities.get(0);
        final T NEW = ENTITY.copy().asNew();

        when(getRepository().save(any())).thenReturn(
                ENTITY // post-save result has id
        );

        T result = getService().insert(NEW);

        assertNotNull(result);
        config.verifyEntity(ENTITY, result);

        verify(getRepository(), times(0)).findById(any());
        verify(getRepository(), times(1)).save(eq(NEW));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    protected void testInsertConstraintViolationException() {
        final ConstraintViolatingEntity<T> cve = config.createConstraintViolatingEntity();
        assertConstraintViolationException(cve.property(), cve.message(), () -> getService().insert(cve.entity()));
    }

    @Test
    void testUpdate() {
        final T ENTITY = entities.get(0);
        final T UPDATE = config.transformForUpdate(ENTITY);

        when(getRepository().findById(eq(UPDATE.getId()))).thenReturn(
                Optional.of(ENTITY)
        );
        when(getRepository().save(any())).then(
                AdditionalAnswers.returnsFirstArg()
        );

        T result = getService().update(UPDATE);

        assertNotNull(result);
        config.verifyEntity(UPDATE, result);

        verify(getRepository(), times(1)).findById(eq(UPDATE.getId()));
        verify(getRepository(), times(1)).save(any());
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    void testUpdateWithIdNotFound() {
        final String UNKNOWN_ID = "UNKNOWN";
        final T ENTITY = entities.get(0).withId(UNKNOWN_ID);

        when(getRepository().findById(eq(UNKNOWN_ID))).thenReturn(
                Optional.empty()
        );

        EntityNotFoundException enfe = assertThrows(EntityNotFoundException.class, () ->
                getService().update(ENTITY)
        );

        assertEquals(UNKNOWN_ID, enfe.getId());

        verify(getRepository(), times(1)).findById(eq(UNKNOWN_ID));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    protected void testUpdateConstraintViolationException() {
        final ConstraintViolatingEntity<T> cve = config.createConstraintViolatingEntity();
        assertConstraintViolationException(cve.property(), cve.message(), () -> getService().update(cve.entity()));
    }

    @Test
    void testDelete() {
        final T ENTITY = entities.get(0);
        final String ID = ENTITY.getId();

        getService().delete(ID);

        verify(getRepository(), times(1)).deleteById(eq(ID));
        verifyNoMoreInteractions(getRepository());
    }

    @Test
    void testDeleteConstraintViolationException() {
        assertConstraintViolationException("delete.id", "must not be blank", () ->
                getService().delete(null)
        );
        assertConstraintViolationException("delete.id", "must not be blank", () ->
                getService().delete(" ")
        );
    }

    @Test
    void testDeleteAll() {
        getService().deleteAll();

        verify(getRepository(), times(1)).deleteAll();
    }

    @Test
    void testThrowDataAccessException() {
        DataAccessException dae = assertThrows(DataAccessException.class, () ->
                getService().throwDataAccessException()
        );

        assertTrue(dae instanceof DataIntegrityViolationException);
        assertEquals("DataAccessException for health monitoring & testing purposes", dae.getMessage());
    }

    protected void assertConstraintViolationException(String propertyPath, String message, Executable executable) {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class, executable);

        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertNotNull(violations);
        assertEquals(violations.size(), 1);

        ConstraintViolation<?> violation = violations.iterator().next();

        String actualPropertyPath = violation.getPropertyPath().toString();
        assertNotNull(actualPropertyPath);
        assertTrue(actualPropertyPath.endsWith(propertyPath));
        assertEquals(message, violation.getMessage());

        verifyNoInteractions(getRepository());
    }
}
