package com.codegap.proto.microshop.customer.controller.impl;

import com.codegap.proto.microshop.customer.controller.ApiUri;
import com.codegap.proto.microshop.customer.controller.CrudControllerTestConfig;
import com.codegap.proto.microshop.customer.controller.PagingCrudControllerTest;
import com.codegap.proto.microshop.customer.model.ConstraintViolatingEntity;
import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.service.impl.CustomerService;
import lombok.Getter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@WebMvcTest(CustomerController.class)
class CustomerControllerTest extends PagingCrudControllerTest<Customer> {
    @MockBean
    @Getter
    CustomerService service;

    @MockBean
    @Getter
    CustomerFinderResolver finderResolver;

    @TestConfiguration
    static class TestConfig {
        @Bean
        public CrudControllerTestConfig<Customer> config() {
            return new CrudControllerTestConfig<Customer>()
                    .withBaseUri(ApiUri.CUSTOMER_V1)
                    .withEntitySupplier(() -> List.of(
                            Customer.of("1", "Barney", "Box", "barney@box.com", "BARNEY"),
                            Customer.of("2", "Courtney", "Cox", "courtney@cox.com", "COURTNEY"),
                            Customer.of("3", "Eddy", "Eagle", "eddy@eagle.com", "EDDY")
                    ))
                    .withConstraintViolatingEntitySupplier(() ->
                            new ConstraintViolatingEntity<>("firstname", "must not be blank", Customer.of("1", null, "Box", "barney@box.com", "BARNEY"))
                    );
        }
    }

    @Test
    void testFindAllSearchOnLastnameContaining() throws Exception {
        final Customer ENTITY_0 = getEntities().get(0);
        final Customer ENTITY_1 = getEntities().get(1);
        final String SEARCH = "ox";

        when(finderResolver.resolve(eq(SEARCH), any(Pageable.class))).thenReturn(() ->
                service.findAllByLastnameContaining(SEARCH, DEFAULT_PAGEABLE)
        );
        when(service.findAllByLastnameContaining(eq(SEARCH), any(Pageable.class))).thenReturn(
                new PageImpl<>(List.of(ENTITY_0, ENTITY_1))
        );

        findAllImpl("?search=" + SEARCH, 2, 2, 0, ENTITY_0.getId());
    }

    @Test
    void testFindAllSearchOnEmailContaining() throws Exception {
        final Customer ENTITY = getEntities().get(0);
        final String SEARCH = ENTITY.getEmail();

        when(finderResolver.resolve(eq(SEARCH), any(Pageable.class))).thenReturn(() ->
                service.findAllByEmailContaining(SEARCH, DEFAULT_PAGEABLE)
        );
        when(service.findAllByEmailContaining(eq(SEARCH), any(Pageable.class))).thenReturn(
                new PageImpl<>(List.of(ENTITY))
        );

        findAllImpl("?search=" + SEARCH, 1, 1, 0, ENTITY.getId());
    }

}