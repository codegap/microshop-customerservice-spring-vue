package com.codegap.proto.microshop.customer.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
public class CustomerTest {
    private static final LocalDateTime DATE_1 = LocalDateTime.of(2010, 1, 1, 11, 11);
    private static final LocalDateTime DATE_2 = LocalDateTime.of(2020, 2, 2, 22, 22);
    private static final Customer CUSTOMER_1 =
            Customer.builder().id("1")
                    .firstname("111")
                    .lastname("11111")
                    .email("1@email.com")
                    .password("password1")
                    .createdDate(DATE_1)
                    .lastModifiedDate(DATE_1)
                    .build();
    private static final Customer CUSTOMER_2 =
            Customer.builder().id("2")
                    .firstname("222")
                    .lastname("22222")
                    .email("2@email.com")
                    .password("password2")
                    .createdDate(DATE_2)
                    .lastModifiedDate(DATE_2)
                    .build();

    @Test
    void testMergeForUpdate() {
        Customer result = CUSTOMER_1.merge(CUSTOMER_2);
        assertNotNull(result);
        assertEquals("1", result.getId());
        assertEquals("222", result.getFirstname());
        assertEquals("22222", result.getLastname());
        assertEquals("2@email.com", result.getEmail());
        assertEquals("password1", result.getPassword());
        assertEquals(DATE_1, result.getCreatedDate());
        assertEquals(DATE_1, result.getLastModifiedDate());
    }
}
