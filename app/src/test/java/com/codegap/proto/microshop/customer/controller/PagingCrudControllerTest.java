package com.codegap.proto.microshop.customer.controller;

import com.codegap.proto.microshop.customer.exception.EntityNotFoundException;
import com.codegap.proto.microshop.customer.model.ConstraintViolatingEntity;
import com.codegap.proto.microshop.customer.model.Customer;
import com.codegap.proto.microshop.customer.model.PersistableEntity;
import com.codegap.proto.microshop.customer.service.PagingCrudService;
import lombok.Getter;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public abstract class PagingCrudControllerTest<T extends PersistableEntity<T>> extends ControllerTest {
    protected static final Pageable DEFAULT_PAGEABLE = PageRequest.of(0, 5, Customer.SORT_DEFAULT);

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected CrudControllerTestConfig<T> config;

    protected abstract PagingCrudService<T> getService();

    protected abstract FinderResolver<T> getFinderResolver();

    @Getter
    private List<T> entities;

    @BeforeEach
    void initEntities() {
        this.entities = config.createEntities();
    }

    @Test
    void testFindAll() throws Exception {
        final T ENTITY = getEntities().get(2);

        when(getFinderResolver().resolve(isNull(), any(Pageable.class))).thenReturn(() ->
                getService().findAll(DEFAULT_PAGEABLE)
        );
        when(getService().findAll(any(Pageable.class))).thenReturn(
                new PageImpl<>(getEntities())
        );

        findAllImpl("", 3, 3, 2, ENTITY.getId());
    }

    @Test
    void testFindAllNoResult() throws Exception {
        when(getFinderResolver().resolve(isNull(), any(Pageable.class))).thenReturn(() ->
                getService().findAll(DEFAULT_PAGEABLE)
        );
        when(getService().findAll(any(Pageable.class))).thenReturn(
                new PageImpl<>(List.of())
        );

        test(get(config.getBaseUri()))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.meta.items").value(0))
                .andExpect(jsonPath("$.meta.pages").value(1))
                .andExpect(jsonPath("$.meta.current").value(1))
                .andExpect(jsonPath("$.content", Matchers.hasSize(0)));
    }

    @Test
    void testFindAllPage1() throws Exception {
        final T ENTITY_0 = getEntities().get(0);
        final T ENTITY_1 = getEntities().get(1);
        final PageRequest PAGE_REQ = PageRequest.of(0, 2);

        when(getFinderResolver().resolve(isNull(), any(Pageable.class))).thenReturn(() ->
                getService().findAll(PAGE_REQ)
        );
        when(getService().findAll(eq(PAGE_REQ))).thenReturn(
                new PageImpl<>(List.of(ENTITY_0, ENTITY_1), PAGE_REQ, 3)
        );

        findAllImpl("?page=1&size=2", 3, 2, 0, ENTITY_0.getId());
    }

    @Test
    void testFindAllPage2() throws Exception {
        final T ENTITY_2 = getEntities().get(2);
        final PageRequest PAGE_REQ = PageRequest.of(1, 2);

        when(getFinderResolver().resolve(isNull(), any(Pageable.class))).thenReturn(() ->
                getService().findAll(PAGE_REQ)
        );
        when(getService().findAll(eq(PAGE_REQ))).thenReturn(
                new PageImpl<>(List.of(ENTITY_2), PAGE_REQ, 1)
        );

        findAllImpl("?page=2&size=2", 3, 1, 0, ENTITY_2.getId());
    }

    protected void findAllImpl(String url, int totalItems, int resultSize, int index, String id) throws Exception {
        test(get(config.getBaseUri() + url))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.meta.items").value(totalItems))
                .andExpect(jsonPath("$.content", Matchers.hasSize(resultSize)))
                .andExpect(jsonPath("$.content.[" + index + "].id").value(id));
    }

    @Test
    void testGetById() throws Exception {
        final T ENTITY = getEntities().get(0);

        when(getService().getById(eq(ENTITY.getId()))).thenReturn(
                ENTITY
        );

        test(get(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(ENTITY.getId()));
    }

    @Test
    void testGetByIdForUnknownId() throws Exception {
        final String UNKNOWN_ID = "999";

        when(getService().getById(eq(UNKNOWN_ID))).thenThrow(
                new EntityNotFoundException(UNKNOWN_ID)
        );

        test(get(ApiUri.getIdentityUri(config.getBaseUri(), UNKNOWN_ID)))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Entity not found for id '%s'".formatted(UNKNOWN_ID)))
                .andExpect(jsonPath("$.debugMessage").doesNotExist());
    }

    @Test
    void testCreate() throws Exception {
        final T ENTITY = getEntities().get(0);
        final String JSON = mapToJson(ENTITY.copyAsNew());

        when(getService().insert(any())).thenReturn(
                ENTITY
        );

        test(post(config.getBaseUri()).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", Matchers.endsWithIgnoringCase(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY))))
                .andExpect(jsonPath("$").doesNotExist()); // empty body
    }

    @Test
    void testCreateWithValidationError() throws Exception {
        ConstraintViolatingEntity<T> cve = config.createConstraintViolatingEntity();
        final T ENTITY = cve.entity();
        final String JSON = mapToJson(ENTITY);

        test(post(config.getBaseUri()).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Method argument not valid"))
                .andExpect(jsonPath("$.debugMessage").value(Matchers.startsWith("""
                        org.springframework.web.bind.MethodArgumentNotValidException: Validation failed for argument""")))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field").value(cve.property()))
                .andExpect(jsonPath("$.fieldErrors[0].message").value(cve.message()));

        verifyNoInteractions(getService());
    }

    @Test
    void testCreateWithoutBody() throws Exception {
        test(post(config.getBaseUri()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Malformed JSON request"))
                .andExpect(jsonPath("$.debugMessage").value(Matchers.startsWith("""
                        org.springframework.http.converter.HttpMessageNotReadableException: Required request body is missing""")));

        verifyNoInteractions(getService());
    }

    @Test
    void testCreateBatch() throws Exception {
        final String JSON = mapToJson(
                getEntities().stream().map(T::asNew).toList()
        );

        test(post(ApiUri.getBatchUri(config.getBaseUri())).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").doesNotExist()); // empty body

        verify(getService(), times(1)).insertAll(any());
    }

    @Test
    void testUpdate() throws Exception {
        final T ENTITY = getEntities().get(0);
        final String JSON = mapToJson(ENTITY);

        when(getService().update(any())).thenReturn(
                ENTITY
        );

        test(put(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY)).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(ENTITY.getId()));
    }

    @Test
    void testUpdateWithUnknownId() throws Exception {
        final String UNKNOWN_ID = "999";
        final T ENTITY = getEntities().get(0).setId(UNKNOWN_ID);
        final String JSON = mapToJson(ENTITY);

        when(getService().update(any())).thenThrow(
                new EntityNotFoundException(UNKNOWN_ID)
        );

        test(put(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY)).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Entity not found for id '%s'".formatted(UNKNOWN_ID)))
                .andExpect(jsonPath("$.debugMessage").doesNotExist());
    }

    @Test
    void testUpdateWithValidationErrors() throws Exception {
        ConstraintViolatingEntity<T> cve = config.createConstraintViolatingEntity();
        final T ENTITY = cve.entity();
        final String JSON = mapToJson(ENTITY);

        test(put(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY)).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Method argument not valid"))
                .andExpect(jsonPath("$.debugMessage").value(Matchers.startsWith("""
                        org.springframework.web.bind.MethodArgumentNotValidException: Validation failed for argument""")))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field").value(cve.property()))
                .andExpect(jsonPath("$.fieldErrors[0].message").value(cve.message()));

        verifyNoInteractions(getService());
    }

    @Test
    void testUpdateWithoutBody() throws Exception {
        final T ENTITY = getEntities().get(0);

        test(put(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Malformed JSON request"))
                .andExpect(jsonPath("$.debugMessage").value(Matchers.startsWith("""
                        org.springframework.http.converter.HttpMessageNotReadableException: Required request body is missing""")));

        verifyNoInteractions(getService());
    }

    @Test
    void testUpdateDataAccessException() throws Exception {
        final T ENTITY = getEntities().get(0);
        final String JSON = mapToJson(ENTITY);

        when(getService().update(any())).thenThrow(
                new DataIntegrityViolationException("Some horrible data access error")
        );

        test(put(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY)).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").value("Error accessing data"))
                .andExpect(jsonPath("$.debugMessage").value("""
                        org.springframework.dao.DataIntegrityViolationException: Some horrible data access error"""));
    }

    @Test
    void testDelete() throws Exception {
        final T ENTITY = getEntities().get(0);

        test(delete(ApiUri.getIdentityUri(config.getBaseUri(), ENTITY)))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist()); // empty body
    }

    @Test
    void testDeleteBatch() throws Exception {
        final String JSON = mapToJson(
                getEntities().stream().map(T::getId).toList()
        );

        test(delete(ApiUri.getBatchUri(config.getBaseUri())).content(JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$").doesNotExist()); // empty body

        verify(getService(), times(1)).deleteAll(any());
    }

    @Test
    void testDeleteAll() throws Exception {
        test(delete(config.getBaseUri()))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$").doesNotExist()); // empty body

        verify(getService(), times(1)).deleteAll();
    }

    @Test
    void testError() throws Exception {
        doThrow(
                new DataIntegrityViolationException("Some horrible data access error")
        ).when(getService()).throwDataAccessException();

        test(get(ApiUri.getErrorUri(config.getBaseUri())))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").value("Error accessing data"))
                .andExpect(jsonPath("$.debugMessage").value("""
                        org.springframework.dao.DataIntegrityViolationException: Some horrible data access error"""));
    }

    protected ResultActions test(MockHttpServletRequestBuilder req) throws Exception {
        return mockMvc.perform(req)
                .andDo(print())
                .andExpect(content().encoding("UTF-8"));
    }
}
