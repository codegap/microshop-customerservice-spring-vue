package com.codegap.proto.microshop.customer.controller;

import com.codegap.proto.microshop.customer.model.ConstraintViolatingEntity;
import com.codegap.proto.microshop.customer.model.PersistableEntity;

import java.util.List;
import java.util.function.Supplier;

public class CrudControllerTestConfig<T extends PersistableEntity<T>> {
    private String baseUri;
    private Supplier<List<T>> entitySupplier;
    private Supplier<ConstraintViolatingEntity<T>> constraintViolatingEntitySupplier;

    public CrudControllerTestConfig<T> withBaseUri(String baseUri) {
        this.baseUri = baseUri;
        return this;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public CrudControllerTestConfig<T> withEntitySupplier(Supplier<List<T>> entitySupplier) {
        this.entitySupplier = entitySupplier;
        return this;
    }

    public List<T> createEntities() {
        return entitySupplier.get();
    }

    public CrudControllerTestConfig<T> withConstraintViolatingEntitySupplier(Supplier<ConstraintViolatingEntity<T>> constraintViolatingEntitySupplier) {
        this.constraintViolatingEntitySupplier = constraintViolatingEntitySupplier;
        return this;
    }

    public ConstraintViolatingEntity<T> createConstraintViolatingEntity() {
        return constraintViolatingEntitySupplier.get();
    }
}
