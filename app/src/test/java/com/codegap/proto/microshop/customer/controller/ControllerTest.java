package com.codegap.proto.microshop.customer.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

public abstract class ControllerTest {
    // Prevents an error when setting up the tests
    @MockBean
    org.springframework.data.mongodb.core.convert.MappingMongoConverter mappingMongoConverter;

    protected String mapToJson(Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }
    protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException {
        return new ObjectMapper().readValue(json, clazz);
    }
}
