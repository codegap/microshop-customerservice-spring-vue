package com.codegap.proto.microshop.customer.service;

import com.codegap.proto.microshop.customer.model.ConstraintViolatingEntity;
import com.codegap.proto.microshop.customer.model.PersistableEntity;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class CrudServiceTestConfig<T extends PersistableEntity<T>> {
    private Supplier<List<T>> entitySupplier;
    private Supplier<ConstraintViolatingEntity<T>> constraintViolatingEntitySupplier;
    private BiConsumer<T, T> entityVerifier;
    private Function<T, T> updateTransformer;

    public CrudServiceTestConfig<T> withEntitySupplier(Supplier<List<T>> entitySupplier) {
        this.entitySupplier = entitySupplier;
        return this;
    }

    public List<T> createEntities() {
        return entitySupplier.get();
    }

    public CrudServiceTestConfig<T> withConstraintViolatingEntitySupplier(Supplier<ConstraintViolatingEntity<T>> constraintViolatingEntitySupplier) {
        this.constraintViolatingEntitySupplier = constraintViolatingEntitySupplier;
        return this;
    }

    public ConstraintViolatingEntity<T> createConstraintViolatingEntity() {
        return constraintViolatingEntitySupplier.get();
    }

    public CrudServiceTestConfig<T> withEntityVerifier(BiConsumer<T, T> entityVerifier) {
        this.entityVerifier = entityVerifier;
        return this;
    }

    public void verifyEntity(T expected, T actual) {
        entityVerifier.accept(expected, actual);
    }

    public CrudServiceTestConfig<T> withUpdateTransformer(Function<T, T> updateTransformer) {
        this.updateTransformer = updateTransformer;
        return this;
    }

    protected T transformForUpdate(T template) {
        return updateTransformer.apply(template);
    }
}
